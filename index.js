const express = require('express');
const app = express();
const path = require('path');
const morgan = require('morgan');
const fs = require('fs').promises;

app.use(express.json());
app.use(morgan('tiny'));

const createFile = async (req, res) => {
    try {
        const { filename, content } = req.body;
        if (content === '' || !content) {
            return res.status(400).json({message: 'Please specify "content" parameter'});
        } 
        const ext = /\S+\.(log|txt|gif|json|yaml|xml|js)$/gi;
        const isValid = ext.test(filename);
        if (!isValid) {
            return res.status(400).json({message: 'Extension is not support'});
        }
        await fs.writeFile(`./api/files/${filename}`, `${content}`, 'utf8');
        return res.status(200).json({message: 'File created successfully'});  
    } catch (err) {
        throw err;
    }
}

const getFiles = async (req, res) => {
    try {
        const foldersArr = await fs.readdir('./api');
        if (!foldersArr.find(item => item === 'files')) {
            return res.status(400).json({message: 'Client error'});
        }
        const files = await fs.readdir('./api/files');
        return res.status(200).json({message: 'Success', files});
    } catch (err) {
        throw err;
    } 
}

const getFile = async (req, res) => {
    try {
        const files = await fs.readdir('./api/files');
        if (!files.find(item => item === req.params.filename)) {
            return res.status(400).json({message: `No file with ${req.params.filename} filename found`});
        }
        const file = await path.parse(req.params.filename);
        const content = await fs.readFile(`./api/files/${req.params.filename}`, 'utf8');
        const uploadedDate = await fs.stat(`./api/files/${file.base}`, (err, stats) => {
            return stats;
        });
        const extension = (file.ext).split('.');
        return res.status(200).json({
            message: 'Success',
            filename: req.params.filename,
            content: content,
            extension: extension[extension.length - 1],
            uploadedDate: uploadedDate.birthtime
        });  
    } catch (err) {  
        throw err;
    } 
}

app.post('/api/files', createFile);

app.get('/api/files', getFiles);

app.get('/api/files/:filename', getFile);

app.use((req, res) => {
    return res.status(500).json({message: 'Server error'});
});

app.listen(8080);